// Loader.cpp : Defines the entry point for the console application.
#include <windows.h> 
#include "stdio.h"

typedef void(__cdecl *OnStartProcedure)();

int main()
{
    HINSTANCE hModule = LoadLibrary(TEXT("Client.dll"));

    OnStartProcedure pOnStart = (OnStartProcedure)GetProcAddress(hModule, "OnStart");
    //TODO: declare another procedure for draw
    //TODO: declare another procedure for close

    if (NULL == pOnStart)
        return -1;

    pOnStart();

    //TODO: call draw procedure 10 times

    //TODO: call close procedure

    FreeLibrary(hModule);

    return 0;
}
